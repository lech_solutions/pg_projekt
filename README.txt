Das Projekt, welches wir in den kommenden Monaten entwickeln werdne tr�gt
den simplen Namen "Mastermind". Dies wird eine digitale Portierung genau
jenen Spiels, dass bereits Anfang der 70er Jahr erschienen ist.

Bei dem Spiel geht es darum, einen vorher festgelegten Code zu erraten. 
Dieser besteht aus einer Reihenfolge verschiedenfarbiger Markierer, in 
unserem Fall simple Kreise des BoS. Das Spielfeld ist nach unten hin 
begrenzt. Err�t man den Code nicht bis erreichen des Spieldfeldrandes, 
hat man verloren. Nach jeder Runde bekommt man R�ckmeldung, welche Felder
man korrekt erraten hat, wenn nein ob die Farbe zumindest in der gesuchten
Kombination vorkommt oder ob keines von Beidem der Fall ist (Ampelsystem)
Der Schwie rigkeitsgrad steigt mit wachsender Codel�nge, damit aber auch 
die Anzahl der vorhandenen Versuche. Vorgesehen sind Kombinationen der 
maximalen L�nge 10 - 12. Der Schwierigkeitsgrad ist anpassbar. 

Im BoS selbst, wird es einen Button geben, mit welchem man den aktuellen Tipp
best�tigen kann. Auch ein "Aufgeben" Button wird es geben. Die nicht erratene
Kombination wird nach dem letzten Versuch angezeigt, ebenso nachdem der Spieler
aufgegeben hat. Auch hat man die M�glichkeit, das aktuelle Spiel neuzustarten.

Die gr��te technische Herausforderung sehen wir unter anderem in der Fehlerfreiheit des Programms.
Beispielsweise sollen keine Felder angesteuert werden, die erst f�r den �bern�chsten
Tipp gedacht sind. Auch sollte man beim Neustarten des Spiels den Schwierigkeitsgrad
�ndern k�nnen, ohne das gesamte Programm neustarten zu m�ssen. Der Schl�ssel zum 
erreichen dieser Ziele liegt vor allem im ausgiebigen Testen. Es ist immer davon
auszugehen, dass der Nutzer nicht das macht, was er soll. Aus diesem Grund ist etwaigen
"Eigenarten" des Spiels vorzubeugen. 

