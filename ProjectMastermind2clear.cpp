// ProjectMastermind2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <time.h>
int abfrage;
void startGame();
// Definieren von Hilfsfunktionen im folgenden Bereich

int getColor(int number)
{
	int farbe;
	switch (number)
	{
	case 0:
		farbe = DARKBLUE;
		break;
	case 1:
		farbe = DARKCYAN;
		break;
	case 2:
		farbe = FUCHSIA;
		break;
	case 3:
		farbe = DARKGREEN;
		break;
	case 4:
		farbe = DARKVIOLET;
		break;
	case 5:
		farbe = DARKGOLDENROD;
		break;
	case 6:
		farbe = DARKRED;
		break;
	}

	//printf("Farbe: %d\n", farbe);
	return farbe;
}

int * createCode(int difficulty)
{
	int farben[] = { 1,2,3,4,5,6,7 };
	int randomNumber;
	srand((unsigned)time(NULL));

	// randomNumber = rand() % (6 - 0 + 1) +0;


	int i;
	if (difficulty == 1)
	{
		static int code[5];
		for (i = 0; i< (sizeof(code) / sizeof(code[0])); i++)
		{
			randomNumber = rand() % 7;
			code[i] = getColor(randomNumber);
			printf("within function. %d\n", code[i]);

		}
		return code;

	}

	else if (difficulty == 2)
	{
		static int code[6];
		for (i = 0; i< (sizeof(code) / sizeof(code[0]) - 1); i++)
		{
			randomNumber = rand() % 7;
			code[i] = getColor(randomNumber);

		}
		for (i = 0; i< (sizeof(code) / sizeof(code[0])); i++)
		{
			randomNumber = rand() % 7;
			code[i] = getColor(randomNumber);
			printf("within function. %d\n", code[i]);

		}
		return code;
	}

	else if (difficulty == 3)
	{
		static int code[7];
		for (i = 0; i < (sizeof(code) / sizeof(code[0])) - 1; i++)
		{
			randomNumber = rand() % 7;
			code[i] = getColor(randomNumber);

		}
		for (i = 0; i< (sizeof(code) / sizeof(code[0])); i++)
		{
			randomNumber = rand() % 7;
			code[i] = getColor(randomNumber);
			printf("within function. %d\n", code[i]);

		}
		return code;

	}


}


void startscreen()
{
	groesse(21, 18);
	flaeche(GRAY);
	formen("s");
	farben(GRAY);
	rahmen(GRAY);

	char easy[] = "EASY";
	char medium[] = "MEDIUM";
	char difficult[] = "DIFFICULT";
	text(239, easy);
	text(241, medium);
	text(243, difficult);


}

void explain() {
	printf("KNACKE DEN FARBENCODE!\n\n*Waehle deine Schwierigkeitsstufe.\n EASY=Codelaenge 5\nMEDIUM=Codelaenge 6\nDIFICULT=Codelaenge 7\n\n*Gib deinen Tipp in der obersten weissen Reihe im Spielfeld links ab.\nDie Farbe eines Kreises aendert sich durch anklicken.\nACHTUNG:Die gleiche Farbe kann mehrfacj vorkommen!\n\n*Best�tige deinen Tipp mit dem gr�nen Button rechts.\n\n*Im Pr�ffeld recht erfolgt die Ausgabe.\nROT: Farbe nicht im Code.\nGELB:Farbe im Code enthalten, aber an der falschen Position.\nGRUEN: Farbe und Position korrekt.\n\n*Du hast gewonnen, wenn alle Angaben korrekt sind.\n DuVerlierst, wenn deine Versuche aufgebraucht sind.\n\n*SHOW zeigt dir die Loesung. Das Spiel ist dann automatisch verloren.\n\n*NEW bringt dich zurueck zum Startscreen.\n\nHOW TO hast du schon selbst herausgefunden.Wir sind stolz auf dich.");
}
void clean() {

	char cleaning = 31;
	zeichen(239, cleaning);
	zeichen(241, cleaning);
	zeichen(243, cleaning);
}
void easy()
{
	clean();
	groesse(21, 18);
	flaeche(GRAY);
	formen("s");
	farben(GRAY);
	rahmen(GREEN);
	char field3[] = "CHECK";
	text(345, field3);



	int spielfeld[] = { 44,45,46,47,48,65,66,67,68,69,86,87,88,89,90,107,108,109,110,111,128,129,130,131,132,149,150,151,152,153,170,171,172,173,174,191,192,193,194,195,212,213,214,215,216,233,234,235,236,237,254,255,256,257,258,275,276,277,278,279,296,297,298,299,300,317,318,319,320,321 };

	int sfeld = sizeof spielfeld / sizeof spielfeld[0];
	int i;

	for (i = 0; i < sfeld; i++)
	{
		farbe(spielfeld[i], WHITE);
		form(spielfeld[i], "c");

	}

	int loesung[] = { 51,52,53,54,55,72,73,74,75,76,93,93,94,95,96,97,114,115,116,117,118,135,136,137,138,139,156,157,158,159,160,177,178,179,180,181,198,199,200,201,202,219,220,221,222,223,240,241,242,243,244,261,262,263,264,265,282,283,284,285,286,303,304,305,306,307,324,325,326,327,328 };
	int lfeld = sizeof loesung / sizeof loesung[0];
	int j;

	for (j = 0; j < lfeld; j++)
	{
		farbe(loesung[j], WHITE);
		form(loesung[j], "c");
	}


	/*farbe(41, DARKBLUE);
	farbe(62, DARKCYAN);
	farbe(83, DARKRED);
	farbe(104, FUCHSIA);
	farbe(125, DARKGOLDENROD);*/




}
void medium()
{
	clean();
	groesse(21, 18);
	flaeche(GRAY);
	formen("s");
	farben(GRAY);
	rahmen(YELLOW);
	char field3[] = "CHECK";
	text(346, field3);

	int spielfeld[] = { 44,45,46,47,48,49,65,66,67,68,69,70,86,87,88,89,90,91,107,108,109,110,111,112,128,129,130,131,132,133,149,150,151,152,153,154,170,171,172,173,174,175,191,192,193,194,195,196,212,213,214,215,216,217,233,234,235,236,237,238,254,255,256,257,258,259,275,276,277,278,279,280,296,297,298,299,300,301,317,318,319,320,321,322 };
	int sfeld = sizeof spielfeld / sizeof spielfeld[0];
	int i;

	for (i = 0; i < sfeld; i++)
	{
		farbe(spielfeld[i], WHITE);
		form(spielfeld[i], "c");

	}

	int loesung[] = { 52,53,54,55,56,57,73,74,75,76,77,78,94,95,96,97,98,99,115,116,117,118,119,120,136,137,138,139,140,141,157,158,159,160,161,162,178,179,180,181,182,183,199,200,201,202,203,204,220,221,222,223,224,225,241,242,243,244,245,246,262,263,264,265,266,267,283,284,285,286,287,288,304,305,306,307,308,309,325,326,327,328,329,330 };
	int lfeld = sizeof loesung / sizeof loesung[0];
	int j;

	for (j = 0; j < lfeld; j++)
	{
		farbe(loesung[j], WHITE);
		form(loesung[j], "c");
	}


}
void difficult()
{
	clean();
	groesse(21, 18);
	flaeche(GRAY);
	formen("s");
	farben(GRAY);
	rahmen(RED);
	char field3[] = "CHECK";
	text(347, field3);

	int spielfeld[] = { 44,45,46,47,48,49,50,65,66,67,68,69,70,71,86,87,88,89,90,91,92,107,108,109,110,111,112,113,128,129,130,131,132,133,134,149,150,151,152,153,154,155,170,171,172,173,174,175,176,191,192,193,194,195,196,197,212,213,214,215,216,217,218,233,234,235,236,237,238,239,254,255,256,257,258,259,260,275,276,277,278,279,280,281,296,297,298,299,300,301,302,317,318,319,320,321,322,323 };
	int sfeld = sizeof spielfeld / sizeof spielfeld[0];
	int i;

	for (i = 0; i < sfeld; i++)
	{
		farbe(spielfeld[i], WHITE);
		form(spielfeld[i], "c");

	}

	int loesung[] = { 53,54,55,56,57,58,59,74,75,76,77,78,79,80,95,96,97,98,99,100,101,116,117,118,119,120,121,122,137,138,139,140,141,142,143,158,159,160,161,162,163,164,179,180,181,182,183,184,185,200,201,202,203,204,205,206,221,222,223,224,225,226,227,242,243,244,245,246,247,248,263,264,265,266,267,268,269,284,285,286,287,288,289,290,305,306,307,308,309,310,311,326,327,328,329,330,331,332 };
	int lfeld = sizeof loesung / sizeof loesung[0];
	int j;

	for (j = 0; j < lfeld; j++)
	{
		farbe(loesung[j], WHITE);
		form(loesung[j], "c");
	}


}
/*Die Verschiedenen Schwierigkeitsgrade sind in 3 Stufen vordeffiniert und m�ssen in main nur noch aufgerufen werden.
Aufruf per Mausklink in BoS. Angelegt als felder. */

void getActiveFields(int *currentArray, int difficulty)
{
	int t = 0;
	while (t < difficulty + 4)
	{
		printf("\nArraywerteBefore: %d", *(currentArray + t));
		t++;
	}
	int i = 0;
	if (difficulty == 1)
	{
		while (i < 5)
		{
			*(currentArray + i) -= 21;
			i++;
		}
	}
	else if (difficulty == 2)
	{
		while (i < 6)
		{
			*(currentArray + i) -= 21;
			i++;
		}
	}
	else if (difficulty == 3)
	{
		while (i < 7)
		{
			*(currentArray + i) -= 21;
			i++;
		}
	}

	t = 0;
	while (t < difficulty + 4)
	{
		printf("\nArraywerteAFTER: %d", *(currentArray + t));
		t++;
	}

}

int changeColor(int color)
{


	int newColor = BLACK;
	if (color == WHITE)
	{
		newColor = DARKBLUE;
	}
	else if (color == DARKBLUE)
	{
		newColor = DARKCYAN;
	}
	else if (color == DARKCYAN)
	{
		newColor = FUCHSIA;
	}

	else if (color == FUCHSIA)
	{
		newColor = DARKGREEN;
	}

	else if (color == DARKGREEN)
	{
		newColor = DARKVIOLET;
	}
	else if (color == DARKVIOLET)
	{
		newColor = DARKGOLDENROD;
	}
	else if (color == DARKGOLDENROD)
	{
		newColor = DARKRED;
	}
	else if (color == DARKRED)
	{
		newColor = DARKBLUE;
	}

	return newColor;
}

void endgame(bool won)
{
	if (won)
	{

		groesse(20, 20);
		//formen("s");
		farben(GRAY);




		int i;
		int x;
		int y;
		int a = 20;
		int b = 20;

		for (i = 0; i < a*b; i++) {
			x = i%a;
			y = i / b;
			if ((x - a / 2)*(x - a / 2) + (y - (b / 2))*(y - (b / 2)) < (a / 4)*(a / 4)) {
				form(i, "c");
				form(i + 1, "c");
				form(i - 1, "c");
				form(i + 20, "c");
				form(i - 20, "c");
				farbe(i, YELLOW);
				farbe(i + 1, YELLOW);
				farbe(i - 1, YELLOW);
				farbe(i + 20, YELLOW);
				farbe(i - 20, YELLOW);

			}
		}
		int c = 0;
		for (c; c < 3; c++) {
			int augel;
			farbe(augel = 207, BLACK);
			farbe(augel + 1, BLACK);
			farbe(augel + 20, BLACK);
			farbe(augel + 21, BLACK);

			int auger;
			farbe(auger = 213, BLACK);
			farbe(auger - 1, BLACK);
			farbe(auger + 19, BLACK);
			farbe(auger + 20, BLACK);

			int mund;
			farbe(mund = 129, BLACK);
			farbe(mund + 1, BLACK);
			farbe(mund + 2, BLACK);

			Sleep(500);

			farbe(augel = 207, BLACK);
			farbe(augel + 2, BLACK);
			farbe(augel + 21, BLACK);
			farbe(augel + 20, YELLOW);
			farbe(augel + 1, YELLOW);

			farbe(auger = 211, BLACK);
			farbe(auger + 21, BLACK);
			farbe(auger + 2, BLACK);
			farbe(auger + 1, YELLOW);
			farbe(auger + 22, YELLOW);

			farbe(mund = 129, BLACK);
			farbe(mund + 1, BLACK);
			farbe(mund + 2, BLACK);
			farbe(mund + 19, BLACK);
			farbe(mund + 38, BLACK);
			farbe(mund + 23, BLACK);
			farbe(mund + 44, BLACK);

			Sleep(500);


			farbe(augel = 207, BLACK);
			farbe(augel + 1, BLACK);
			farbe(augel + 20, BLACK);
			farbe(augel + 21, BLACK);
			farbe(augel + 2, YELLOW);

			farbe(auger = 213, BLACK);
			farbe(auger - 1, BLACK);
			farbe(auger + 19, BLACK);
			farbe(auger + 20, BLACK);
			farbe(auger - 2, YELLOW);

			farbe(mund = 129, BLACK);
			farbe(mund + 1, BLACK);
			farbe(mund + 2, BLACK);
			farbe(mund + 19, YELLOW);
			farbe(mund + 38, YELLOW);
			farbe(mund + 23, YELLOW);
			farbe(mund + 44, YELLOW);
		}

		startGame();
	}
	else
	{
		groesse(20, 20);
		//formen(none);
		flaeche(GRAY);

		int i;
		int x;
		int y;
		int a = 20;
		int b = 20;

		for (i = 0; i<a*b; i++) {
			x = i%a;
			y = i / b;
			if ((x - a / 2)*(x - a / 2) + (y - (b / 2))*(y - (b / 2))<(a / 4)*(a / 4)) {
				form(i, "c");
				form(i + 1, "c");
				form(i - 1, "c");
				form(i + 20, "c");
				form(i - 20, "c");
				farbe(i, YELLOW);
				farbe(i + 1, YELLOW);
				farbe(i - 1, YELLOW);
				farbe(i + 20, YELLOW);
				farbe(i - 20, YELLOW);


			}
		}


		int augel;
		farbe(augel = 207, BLACK);
		farbe(augel + 1, BLACK);
		farbe(augel + 20, BLACK);
		farbe(augel + 21, BLACK);

		int auger;
		farbe(auger = 213, BLACK);
		farbe(auger - 1, BLACK);
		farbe(auger + 19, BLACK);
		farbe(auger + 20, BLACK);

		int mund;
		farbe(mund = 129, BLACK);
		farbe(mund + 1, BLACK);
		farbe(mund + 2, BLACK);


		int traene=192;

		farbe(traene, LIGHTBLUE);
		Sleep(250);
		farbe(traene, YELLOW);
		farbe(traene - 20, LIGHTBLUE);
		Sleep(250);
		farbe(traene - 20, YELLOW);
		farbe(traene - 40, LIGHTBLUE);
		Sleep(250);
		farbe(traene - 40, YELLOW);
		farbe(traene - 60, LIGHTBLUE);
		Sleep(250);
		farbe(traene - 60, YELLOW);
		farbe(traene - 80, LIGHTBLUE);
		Sleep(250);
		farbe(traene - 80, YELLOW);
		form(traene - 100, "c");
		farbe(traene - 100, LIGHTBLUE);
		Sleep(250);
		form(traene - 100, "none");
		form(traene - 120, "c");
		farbe(traene - 120, LIGHTBLUE);
		Sleep(250);
		form(traene - 120, "none");
		form(traene - 140, "c");
		farbe(traene - 140, LIGHTBLUE);
		Sleep(250);
		form(traene - 140, "none");
		form(traene - 160, "c");
		farbe(traene - 160, LIGHTBLUE);
		Sleep(250);
		form(traene - 160, "none");
		form(traene - 180, "c");
		farbe(traene - 180, LIGHTBLUE);
		Sleep(250);
		form(traene - 180, "none");

		startGame();
	}
}






void runGame(int difficulty)
{
	char show[] = "SHOW";
	text(377, show);
	char newgame[] = "NEW";
	text(335, newgame);
	char howto[] = "HOW TO";
	text(293, howto);
	char field1[] = "YOUR";
	char field2[] = "CODE";
	text(338, field1);
	text(339, field2);
	


	int confirmButton;
	int abfrage;
	int runde = 1;
	int * pointerToCodeArray = NULL;
	int *activeFields;
	int *enteredCode;
	int *validationArray = NULL;

	int activeFieldsCount = difficulty + 4;

	enteredCode = (int *)malloc(activeFieldsCount * sizeof(int));
	activeFields = (int *)malloc(activeFieldsCount * sizeof(int));
	validationArray = (int *)malloc(activeFieldsCount * sizeof(int));


	int count = 0;
	while (count < activeFieldsCount)
	{
		enteredCode[count] = WHITE;
		count++;
	}


	if (difficulty == 1)
	{
		pointerToCodeArray = createCode(1);
		int farben[5];

		int i = 0;

		while (i < activeFieldsCount)
		{
			activeFields[i] = 317 + i;
			printf("\n\n%d", activeFields[i]);
			i++;
		}


	}

	else if (difficulty == 2)
	{
		pointerToCodeArray = createCode(2);
		int farben[6];
		int hallo = 0;

		int i = 0;
		while (i < activeFieldsCount)
		{
			activeFields[i] = 317 + i;
			i++;
		}

	}

	else if (difficulty == 3)
	{
		pointerToCodeArray = createCode(3);
		int farben[7];


		int i = 0;
		while (i < activeFieldsCount)
		{
			activeFields[i] = 317 + i;
			i++;
		}

	}

	confirmButton = activeFields[activeFieldsCount - 1] + difficulty + 8;
	
	
	farbe(confirmButton, GREEN);
	
	int i = 0;
	for (;;) {
		char *a = abfragen();
		if (strlen(a) > 0) {
			printf("Nachricht: %s\n", a);
			if (a[0] == '#') {
				sscanf_s(a, "#%d", &abfrage);
				int z = 0;
				for (z = 0; z < activeFieldsCount; z++)
				{
					if (abfrage == activeFields[z])
					{

						int newColor = changeColor(enteredCode[z]);
						farbe(abfrage, newColor);
						enteredCode[z] = newColor;

						int some = 0;
						while (some < activeFieldsCount)
						{
							printf("\n fieldColors[%d] = %d ", z, enteredCode[some]);
							some++;
						}
						break;
					}
				}

				if (abfrage == 377)
				{
					if (difficulty == 1)
					{
						farbe(357, *(pointerToCodeArray));
						farbe(359, *(pointerToCodeArray + 1));
						farbe(361, *(pointerToCodeArray + 2));
						farbe(363, *(pointerToCodeArray + 3));
						farbe(365, *(pointerToCodeArray + 4));
					}

					else if (difficulty == 2)
					{
						farbe(357, *(pointerToCodeArray));
						farbe(359, *(pointerToCodeArray + 1));
						farbe(361, *(pointerToCodeArray + 2));
						farbe(363, *(pointerToCodeArray + 3));
						farbe(365, *(pointerToCodeArray + 4));
						farbe(367, *(pointerToCodeArray + 5));
					}

					else if (difficulty == 3)
					{
						farbe(357, *(pointerToCodeArray));
						farbe(359, *(pointerToCodeArray + 1));
						farbe(361, *(pointerToCodeArray + 2));
						farbe(363, *(pointerToCodeArray + 3));
						farbe(365, *(pointerToCodeArray + 4));
						farbe(367, *(pointerToCodeArray + 5));
						farbe(369, *(pointerToCodeArray + 6));
					}
					//SHOW	
				}
				else if (abfrage == 335)
				{
					startGame();
				}


				// Aktionen wenn auf den Gr�nen "Runde beenden" button geklickt wurde
				else if (abfrage == confirmButton)
				{


					bool valid = true;
					int i = 0;
					while (i < activeFieldsCount)
					{
						if (enteredCode[i] == WHITE)
						{
							valid = false;
							break;
						}
						i++;
					}

					if (valid)
					{
						int c = 0;
						for (c; c < activeFieldsCount; c++)
						{

							if (enteredCode[c] == *(pointerToCodeArray + c))
							{
								validationArray[c] = GREEN;
							}

							else
							{
								int i = 0;
								for (i; i < activeFieldsCount; i++)
								{
									if (enteredCode[c] == *(pointerToCodeArray + i) && i != c)
									{
										validationArray[c] = YELLOW;
										break;
									}
									else
									{
										validationArray[c] = RED;
									}
								}
							}

							farbe(activeFields[c] + difficulty + 6, validationArray[c]);
						}

						int x = 0;
						bool success = true;
						while (x < activeFieldsCount)
						{
							if (validationArray[x] != GREEN)
							{
								success = false;
								break;
							}
							x++;
						}
						if (success)
						{
							printf("You won the game");
							Sleep(1000);
							endgame(true);
						}
						else if (runde == 14)
						{
							farbe(confirmButton, GRAY);
							Sleep(1000);
							printf("looser");
							endgame(false);
						}
						else
						{
							farbe(confirmButton, GRAY);
							getActiveFields(activeFields, difficulty);
							confirmButton = activeFields[activeFieldsCount - 1] + difficulty + 8;
							farbe(confirmButton, GREEN);
							printf("\nRunde beendet!\n");
							runde++;

							int i = 0;
							while (i < activeFieldsCount)
							{
								enteredCode[i] = WHITE;
								i++;
							}
						}

					}


				}
			}
		}
		else {
			Sleep(100);
		}
	}


}



void startGame()
{

	startscreen();
	printf("showStartScreen");
	int currentDifficulty;
	
	for (;;) {
		char *a = abfragen();
		if (strlen(a) > 0) {
			printf("Nachricht: %s\n", a);
			if (a[0] == '#') {
				sscanf_s(a, "#%d", &abfrage);
				if (abfrage == 239)
				{
					/*char clear[] = "";
					text(239, clear);
					text(241, clear);
					text(243, clear);*/
					currentDifficulty = 1;
					easy();
					/*char clear[1] = {};
					text(239, clear);
					text(241, clear);
					text(243, clear);*/

					break;
				}
				else if (abfrage == 241)
				{
					char clear[1] = {};

					currentDifficulty = 2;
					medium();
					break;
				}
				else if (abfrage == 243)
				{
					char clear[1] = {};

					currentDifficulty = 3;
					difficult();
					break;
				}
			}
		}
		else {
			Sleep(100);
		}
	}
	printf("\nPreparations done! Have fun\n\n");
	runGame(currentDifficulty);
}

void buttons() {
	for (;;) {
		char *a = abfragen();
		if (strlen(a) > 0) {
			printf("Nachricht: %s\n", a);
			if (a[0] == '#') {
				sscanf_s(a, "#%d", &abfrage);
				if (abfrage == 335)
				{
					startGame();
				}
				if (abfrage == 293) 
				{
					explain();
				}
				}
			}
		}
	}

int main()
{

	startGame();


	getchar();
	return 0;
}